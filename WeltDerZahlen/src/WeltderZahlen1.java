/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Kilian Schulz >>
  */

public class WeltderZahlen1 {

  public static void main(String[] args) {
     
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ? 
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 1000000000 ;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3664088;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = 6570;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 150000;  
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 17098242;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    double flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
     
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
     
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    System.out.println("Bewohneranzahl von Berlin:" + bewohnerBerlin);
    System.out.println("Mein Alter in Tagen:" + alterTage);
    System.out.println("Das Gewicht des schwersten Tieres der Erde in Kilogramm:" + gewichtKilogramm);
    System.out.println("Das gr��te Land der Erde in km�:" + flaecheGroessteLand);
    System.out.println("Das kleinste Land der Erde in km�" + flaecheKleinsteLand);
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }       
}
