
public class Konsolenausgabe {
	
	public static void main(String[] args) {
		
	// Aufgabe 1:
	System.out.println("Aufgabe 1:\n");
	System.out.printf("%5s\n%s%7s\n%s%7s\n%5s\n\n", "**", "*","*","*","*","**");
	
	
	//Aufgabe 2:
	System.out.println("Aufgabe 2:\n");
	System.out.printf("%s%3s%19s%3s\n%s%3s%2s%17s%3s\n%s%3s%2s%2s%2s%13s%3s\n%s%3s%2s%2s%2s%13s%3s", "0!","=","=","1","1!","=","1","=","1", "2!","=","1","*","2","=","2");
	
	}
	
}  
       