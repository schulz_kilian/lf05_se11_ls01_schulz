/*
 * Erster kleiner Additionsrechner
 */




public class KleinerRechner1 {

	public static void main(String[] args) {
		
		// 3 Variablen werden deklariert, d.h. dem System bekannt gemacht
		int zahl1, zahl2, ergebnis;
		
		// die Variablen werden initialisiert, d.h. es wird ihnen ein Startwert zugewiesen
		zahl1 = 5;		
		zahl2 = 3;
		
		ergebnis = zahl1 + zahl2; 
		
		System.out.println("Das Ergebnis ist: " + ergebnis);
		
	}

}  